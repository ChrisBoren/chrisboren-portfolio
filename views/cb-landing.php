<!DOCTYPE html>
<html>
	<?php
		$pageDescription = 'The online portfolio and homepage of Chris Boren, Chicago based Designer and Front-End Developer.';
		$pageName = 'Home';
		include_once "./php/cb-global.php";
		include_once "./php/cb-head.php"; ?>
	<body>
		<?php include_once "./php/google-analytics.php";
					include_once "./php/ie.php";
					include_once "./php/js-start.php";
					include_once "./php/cb-navbar.php"; ?>
		<!-- <div class="cb-pageWrap"> -->
		<div class="parallax">
			<div id="group2" class="parallax__group xxcb-size-90">
				<div class="parallax__layer parallax__layer--base ">
					<div class="title cb-main-logo">
						<img src="<?php echo $siteLogo;?>">
						<p>Front-End Developer &amp; Designer<br/>Chicago, Illinois</p>
					</div>
				</div>
				<div class="parallax__layer parallax__layer--back cb-bg-chicago">
					<div class="title"></div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
			<div id="cb-content" class="parallax__group cb-size-50">
				<div class="parallax__layer cb-paralax-top parallax__layer--base">
					<div class="cb-container-main ff-container ">
						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-main-content ff-container">
								<div class="ff-row">
									<div class="ff-12g ff-7-lg">
										<h1>Greetings, weary traveler</h1>
										<p>Welcome to my home on the web. For over 10 years I’ve been hand-coding websites, and although <a href="https://atom.io/" target="_nofollow">the tools</a> have changed a bit since I used <a href="http://www.w3schools.com/html/html_editors.asp" target="_blank">Windows Notepad</a> as my primary text editor, many of the methods have stayed the same while others have transformed entirely. </p>
										<p>My primary skillset is front-end development and website design; incorporating modern graphic design tools, the latest CSS and HTML techniques, and JavaScript frameworks for agile DOM manipulation.</p>
										<p>Although I no longer accept smaller contract work (look into <a href="http://www.wix.com" target="_blank" rel="nofollow">Wix</a>, <a href="http://www.squarespace.com" target="_blank" rel="nofollow">Squarespace</a>, or <a href="https://www.weebly.com/" target="_blank" rel="nofollow">Weebly</a> if you need a quick website for your company) I’m keeping this catalog of my work as both a challenge for me to continue to implement latest techniques, and to catalog my past achievements.</p>
										<p>Have a question or just want to reach out to me?  Try contacting me up on Twitter at <a href="http://twitter.com/ChrisBoren" target="_blank">@ChrisBoren</a>.</p>
									</div>
									<div class="ff-12g ff-5-lg">
										<div class="ff-column cb-portfolio-items">
											<div class="ff-text-center">
												<a href="./web-development">
													<div class="cb-portfolio-item">
														<img src="./img/websites/code-screenshot.jpg"/>
														<h2>Development &amp; Design Portfolio</h2>
													</div>
												</a>
											</div>
											<div class="ff-text-center">
												<a href="./about">
													<div class="cb-portfolio-item">
														<img src="./img/front-me-vr.jpg"/>
														<h2>About Chris</h2>
													</div>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<?php include_once "./php/cb-footer.php"; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- </div> CB Page Wrap -->
		<?php include_once "./php/js-end.php";?>
	</body>
</html>
