<!DOCTYPE html>
<html>
	<?php
		$pageDescription = 'Samples of my graphic design and web design abilities.';
		$pageName = 'Design';
		include_once "./php/cb-global.php";
		include_once "./php/cb-head.php"; ?>
	<body>
		<?php include_once "./php/google-analytics.php";
					include_once "./php/js-start.php";
					include_once "./php/cb-navbar.php"; ?>
		<!-- <div class="cb-pageWrap"> -->
		<div class="parallax">
			<div id="group2" class="parallax__group cb-size-50">
				<div class="parallax__layer parallax__layer--base ">
					<div class="title cb-main-logo">
						<img src="<?php echo $siteLogo;?>">
						<p>Design &amp; Development</p>
					</div>
				</div>
				<div class="parallax__layer parallax__layer--back cb-bg-design">
					<div class="title"></div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
			<div id="cb-content" class="parallax__group cb-size-50">
				<div class="parallax__layer cb-paralax-top parallax__layer--base">
					<div class="cb-container-main ff-container ">
						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-main-content ff-container">
								<div class="ff-row">
									<div class="ff-12g ff-8-lg">
										<h1>Web Development &amp; Design</h1>
										<p>Design and development, although many think go hand-in-hand, are not the same skillset. Not all designers can code, and not all developers can develop graphical assets.  Fortunately, I’ve dabbled a bit in both…
										</p>
										<p>In the websites below, you’ll be able to see the design process from conception to final product, and learn a bit about each project’s specific challenges.  Each of these websites began life as a Photoshop or Illustrator mockup, and was presented to the client before any code was written.
										</p>
										<p>Highlights of the following samples:</p>
										<ul>
											<li>Original graphic wireframe creation</li>
											<li>Mockup to functional and responsive CMS front-end theme</li>
											<li>Vector graphic creation, modification, and implementation in HTML (SVG)</li>
											<li>Unique Challenges</li>
										</ul>

									</div>
									<div class="ff-12g ff-4-lg ff-hidden-sm">
										<div class="cb-sidebar">
										<img class="cb-img-responsive" src="./img/youmacon-illustrator.png"/>
										</div>
									</div>
								</div>
								<!-- Website Design Samples -->
								<div class="ff-row cb-samples-title">
									<h2>Website Design Samples</h2>
								</div>
								<div class="ff-row cb-samples ff-center">
									<?php
									// TODO: Document DB + iterate
										portfolioItem(
											// ID
											'designYouma',
											// Title
											'Youmacon 2016',
											// Image
											'./img/sample-youma2016.png',
											// Secondary Link - use NULL if nothing
											'<a href="http://www.youmacon.com"><i class="fa fa-fw fa-external-link"></i>&nbsp;Live Website</a>',
											// Body of popup
											"<h2>Client Overview</h2><p>Started by a group of friends in the Metro Detroit area in 2005, the anime and gaming convention known as Youmacon has become one of the largest and fastest growing events of its type in North America, outselling the hotel rooms of Superbowl XL and managing to become the first and only single event to fill the Detroit Marriott at the Renaissance Center - the tallest hotel in the western hemisphere - to its maximum capacity. To accommodate it's rapid growth, Youmacon expanded its event to include Downtown Detroit's Cobo Center in 2012.  In 2014 the event had an attendance of nearly 17,000 individuals (or over 45,000 turnstile attendance) over the course of the event.</p>
											<h2>Project Details</h2>
											<p>Since 2011 I have been designing the graphics, assets and website for Youmacon, along with developing its front-end theme.  In 2013 I switched from a custom CMS I had helped co-develop (YENN) to Joomla.  I resigned my Director of Media position with the organization in December 2014, however still remain active in the event developing its promotional assets (flyers, logos, etc..) along with its website.</p>

											<p>As the audience shifted to mobile throughout the past two years, I developed the 2016 website to be more mobile and tablet friendly, incorporating a simpler menu hierarchy than used in previous years.</p>

											<p>This project incorporates graphic design elements from Google’s Material Design language, along with custom letter spacing to be more consistent with previous Youmacon print branding.  The theme colors are based on Youmacon’s 2006 website.</p>

											",
											// Sidebar of popup (optional)
											'<table>
												<tr><img src="./img/sample-youma2016.png" class="cb-img-two-thrid cb-img-center cb-z-depth-2"/></tr>
												<tr>
													<th>Client</th>
													<td>Youmacon Enterprises</td>
												</tr>
												<tr>
													<th>URL</th>
													<td><a href="http://www.youmacon.com/" target="_blank">youmacon.com</a></td>
												</tr>
												<tr>
													<th>Date Complete</th>
													<td>4-1-2016</td>
												</tr>
												<tr>
													<th>Software Used</th>
													<td>

															Atom<br/>
															Adobe Illustrator CC 2015<br/>
															Adobe Lightroom CC 2015.1<br/>
															Adobe Photoshop CC 2015<br/>
															Virtualized Ubuntu LAMP Server<br/>

													</td>
													<tr>
														<th>Web Technologies</th>
														<td>Bootstrap 3<br/>
														Joomla 3.5<br/>
														Font Awesome<br/>
														Google Fonts</td>
													</tr>
											</table>'
										);
										portfolioItem(
										  // ID
										  'designLMT',
										  // Title
										  'Loyola Medicine Transport',
										  // Image
										  './img/sample-loyola.png',
										  // Secondary Link - use NULL if nothing
											'<a href="http://www.loyolamedicinetransport.org"><i class="fa fa-fw fa-external-link"></i>&nbsp;Live Website</a>',
										  // Body of popup
										  "
										<h2>Client Overview</h2>

										<p>In 2015 Loyola Medicine and Community EMS partnered to form a new joint-venture logistics company in the Chicago area— Loyola Medicine Transport.</p>

										<p>Loyola Medicine Transport provides medical transportation to Loyola University Medical Center, Gottlieb Memorial Hospital and all Loyola Medicine ambulatory locations. Transport services include basic and advanced life support ambulances, wheelchair van transportation, lab and courier services and critical care transportation.</p>

										<h2>Project Details</h2>

										<p>As the Multimedia Manager for Parastar & Community EMS, I was tasked with creating all branding materials for our new Joint Venture with Loyola University Health System.  The challenge was creating a website and marketing materials which not only matched the look and feel of Loyola Medicine branding, but also conveyed a similar uniform look to our other 11 EMS companies.</p>

										<p>Although I left the company in late 2015 before the website was complete, I created the Loyola Medicine Transport Joomla theme and deployed the contentless website so my successor could propagate and launch.  In addition to the graphic assets and website theme, I also staged the photoshoot and took the photos using my Canon EOS-M with an EF-S 17-55mm f/2.8 IS USM Lens.  These photos are still used in marketing today.</p>

										<p>The navigation bar of this website was my first use of flexbox, coded with an IE fallback for legacy users (common in the medical industry).</p>

											",
										  // Sidebar of popup (optional)
										  '<table>
												<tr><img src="./img/sample-loyola.png" class="cb-img-two-thrid cb-img-center cb-z-depth-2"/></tr>
												<tr>
													<th>Client</th>
													<td>Loyola Medicine Transport</td>
												</tr>
												<tr>
													<th>URL</th>
													<td><a href="http://www.loyolamedicinetransport.org/" target="_blank">LoyolaMedicineTransport.org</a></td>
												</tr>
												<tr>
													<th>Date Complete</th>
													<td>October 2015</td>
												</tr>
												<tr>
													<th>Software Used</th>
													<td>

															Sublime Text 3<br/>
															Adobe Illustrator CC<br/>
															Adobe Lightroom CC<br/>
															Adobe Photoshop CC<br/>
															Virtualized Ubuntu LAMP Server<br/>

													</td>
													<tr>
														<th>Web Technologies</th>
														<td>Bootstrap 3<br/>
														Joomla 3.5<br/>
														Font Awesome<br/>
														Google Fonts</td>
													</tr>
											</table>'
										);

										portfolioItem(
											// ID
											'designCasazza',
											// Title
											'Casazza Law Offices',
											// Image
											'./img/sample-casazza.png',
											// Secondary Link - use NULL if nothing
											'<a href="http://www.casazzalaw.com" target="_blank" rel="nofollow"><i class="fa fa-fw fa-external-link"></i>&nbsp;Live Website*</a><p class="cb-tinyText">*Website launch pending content delivery from client</p>',
											// Body of popup
											"<h2>Client Overview</h2><p>Based in Mt. Clemens, Michigan for over thirty years, Eugene Casazza specializes in Social Security and disability law, helping clients seek the benefits they are entitled to.</p>
											<h2>Project Details</h2>

											<p>Looking to expand his business, Eugene Casazza commissioned me to create a website to help promote his law practice online, as well as create an outlet for blog articles he would write on future news topics.</p>

											<p>The website was created to be a traditional website using a combination of stock photos and commissioned photos.  Since Gene’s clients are less likely to use modern machines, I incorporated respond.js and JQuery 1 for IE8 and Windows XP support.</p>

											<p>Although the website framework is complete, I am currently waiting for content from the client before allowing the site to be indexed on search engines.  This provides an opportunity to showcase a live project before completion.</p>


											"


											,
											// Sidebar of popup (optional)
											'<table>
												<tr><td colspan="2"><img src="./img/sample-casazza.png" class="cb-img-two-thrid cb-img-center cb-z-depth-2"/></td></tr>
												<tr>
													<th>Client</th>
													<td>Eugene Casazza</td>
												</tr>
												<tr>
													<th>URL</th>
													<td><a href="http://www.casazzalaw.com/" target="_blank">CasazzaLaw.com</a></td>
												</tr>
												<tr>
													<th>Date Complete</th>
													<td>Design Complete: Jan 2016<br/>
													Pending Content from Client</td>
												</tr>
												<tr>
													<th>Software Used</th>
													<td>

															Sublime Text<br/>
															Adobe Illustrator CC<br/>
															Adobe Lightroom CC<br/>
															Adobe Photoshop CC<br/>
															Virtualized Ubuntu LAMP Server<br/>

													</td>
													<tr>
														<th>Web Technologies</th>
														<td>Bootstrap 3<br/>
														Joomla 3<br/>
														Font Awesome<br/>
														Google Fonts</td>
													</tr>
											</table>'
										);
										portfolioItem(
										  // ID
										  'designDMCare',
										  // Title
										  'DMCare Express',
										  // Image
										  './img/sample-dmcare.png',
										  // Secondary Link - use NULL if nothing
										  '<a href="http://www.dmcareexpress.org" target="_blank" ><i class="fa fa-fw fa-external-link"></i>&nbsp;Live Website</a>',
										  // Body of popup
										  "
											<h2>Client Overview</h2>
											<p>DMCare Express is a full service medical transportation company offering ALS, BLS, and Non-Emergency Van (NEV) transportation. We are proud to be the joint venture ambulance company owned by The Detroit Medical Center, one of Michigan’s premiere health systems, as well as Community EMS, the largest not-for-profit EMS provider in the region. These relationships provide a solid foundation for our ability to provide additional ambulance assistance if needed. Furthermore, our response times in Detroit and the surrounding areas have always met or exceeded all of our clients’ needs, and we continue to maintain every ambulance contract we have commenced.</p>
											<h2>Project Details</h2>
											<p>As the Multimedia Manager for Parastar & Community EMS, I was tasked with updating the 8-year-old website for our Joint Venture with the Detroit Medical Center. This challenge was creating a new website that not only matched the branding of the Detroit Medical Center, but also improving on the parent company website’s cluttered design while still incorporating enough elements to make it website recognizable to patients of the DMC.</p>

											<p>Many of the photos used on this website were taking before 2012 and of less than ideal quality, however they were processed and optimized in Photoshop before their use on the DMCare Express website.</p>

											<p>Multiple levels of approval and reworks with the client were completed before the website was launched.  Although my theme and creative assets are still in use, the site has been modified since its launch in 2014.</p>
",
										  // Sidebar of popup (optional)
										  '<table>
												<tr><img src="./img/sample-dmcare.png" class="cb-img-two-thrid cb-img-center cb-z-depth-2"/></tr>
												<tr>
													<th>Client</th>
													<td>DMCare Express<br/>A joint-venture partnership between the Detroit Medical Center and Community EMS</td>
												</tr>
												<tr>
													<th>URL</th>
													<td><a href="http://www.dmcareexpress.org/" target="_blank">DMCareExpress.org</a></td>
												</tr>
												<tr>
													<th>Date Complete</th>
													<td>Early 2014</td>
												</tr>
												<tr>
													<th>Software Used</th>
													<td>

															Sublime Text 3<br/>
															Adobe Illustrator CS5<br/>
															Adobe Photoshop CS5<br/>
															Virtualized Ubuntu LAMP Server<br/>

													</td>
													<tr>
														<th>Web Technologies</th>
														<td>Bootstrap 3<br/>
														Joomla 2<br/>
														Font Awesome<br/>
														Google Fonts</td>
													</tr>
											</table>'
										);

									 ?>
								</div>
							</div>
						</div>

						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-content-secondary ff-container cb-blue-bg">
								<div class="ff-row">
									<div class="ff-12g xxff-8-lg">
										<h1>Front-End Website Development</h1>
										<p><img class="cb-img-right" src="./img/responsive-design-cb-edit-white2.svg"/>Although both website design and front-end web development can be considered an art, taking a design and creating a functional website out of it is much more technical then an initial mockup. Not only is cross-browser compatibility a concern, but now one must ensure proper display across a sea of digital devices.</p>

										<p>Today’s challenges come from creating an experience for the end user to get them the information they need quickly. Fortunately for designers we have an abundance of frameworks such as <a href="http://getbootstrap.com/" target="_blank">Bootstrap</a>, <a href="http://foundation.zurb.com/" target="_blank">Foundation</a>, <a href="http://purecss.io/" target="_blank">Pure</a>, and <a href="https://getmdl.io/" target="_blank">Material Design Lite</a> to help speed up production of a website. The only drawback is sometimes the design can be limited by the scope of these technologies, unless heavy modifications and overrides are taken to the CSS code.</p>

										<p>As we move forward, a new standard known as flexbox seeks to replace the old grid systems and css hacks previously utilized in these frameworks. Phillip Walton has a good example of this on his website: <a href="https://philipwalton.github.io/solved-by-flexbox/" target="_blank">Solved by Flexbox</a>. The only apparent drawback is compatibility with Internet Explorer, which has now been replaced by Microsoft Edge. Fortunately, outside a few legacy applications, support for Internet Explorer can now be safely phased out. As demonstration of the power of flexbox and my understanding of this new standard, this portfolio website was written from the ground up without any of the previously mentioned frameworks.</p>

										<p>In preparing my portfolio, I wanted to demonstrate my understanding of various technologies. This website was written from scratch without a Photoshop wireframe, first using <a href="https://www.sublimetext.com/" target="_blank" >Sublime Text</a> and later <a href="https://atom.io/" target="_blank" >Atom</a>.  In doing this, I’ve created a flexbox based grid framework for use on this and other projects going forward – called <a href="https://github.com/ChrisBoren/FlexForward" target="_blank">FlexForward</a> – to accomplish the layout of this website.  Although FlexForward is still very early in development, I believe it’s strong enough to be used in this portfolio as a demonstration.</p>

										<p>In addition to the HTML, CSS, PHP, and the reusable flexbox grid framework I’ve created for this website, I’ve also borrowed a few open source solutions to demonstrate my ability to integrate with existing code and tutorials.</p>

										<h2>Highlights of this website as an example</h2>
										<ul>
											<li>No front-end framework, all layouts were built from the ground up</li>
											<ul>
												<li>In the process, created my own framework called '<a href="https://github.com/ChrisBoren/FlexForward" target="_blank">FlexForward</a>'</li>
											</ul>
											<li><a href="https://necolas.github.io/normalize.css/" target="_blank">Normalize.css</a> for a standardized presentation across browsers</li>
											<li><a href="https://jquery.com/" target="_blank">jQuery</a> and <a href="https://jqueryui.com/" target="_blank">jQuery UI</a> for effects</li>
											<li><a href="http://getbootstrap.com/javascript/#modals" target="_blank">Bootstrap’s Modal.js</a> for overlay pop-ups</li>
											<li>An implementation of <a href="http://keithclark.co.uk/articles/" target="_blank">Keith Clark’s No-Javascript CSS Parallax</a></li>
											<ul>
												<li>Utilizes 3d Transform rather than adjusting the background in the DOM like a javascript solution would.</li>
												<li>This also allows non resource intensive smooth animations on Android’s Chrome.</li>
												<li>Disabled on Microsoft Edge as a fallback for maximum compatibility.  There is currently an open bug for MS Edge addressing the issues with that browser.</li>
												<li> Also disabled in Mobile Safari (iOS Devices) for best appearance, and disabled in Firefox (mobile and android) for lag issues.  For these devices, 3DCanvas is not activated via CSS (parallax.css).</li>
											</ul>
											<li><a href="https://fortawesome.github.io/Font-Awesome/" target="_blank">Font Awesome</a> icon fonts</li>
										</ul>
										<h2>Portfolio code for review</h2>
										<p>This live website can be downloaded from its git repository on GitLab for your review.  In fact, when updating this website, the server pulls from the master branch.</p>
										<br/>
										<p class="ff-text-center"><a class="btn cb-btn" href="https://gitlab.com/ChrisBoren/chrisboren-portfolio" target="_blank"><i class="fa fa-fw fa-gitlab"></i>&nbsp; Download this Portfolio on GitLab</a></p><br/>

									</div>
								</div>
							</div>
						</div>
						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-content-secondary ff-container">

								<div class="ff-row">
									<div class="ff-12g ff-8-lg">
										<h1>Branding and Graphic Design</h1>
										<p>Going hand-in-hand with Website Design is of course the broader skill of graphic design.  Where many web designers have used graphic design programs such as Adobe Photoshop, Adobe Illustrator and Affinity Designer in their workflows, developing for print using the CMYK color space has its own unique challenges.</p>

										<p>Below are several projects of which I worked with the brand identity of a professional entity, where I worked with or created branding standards, manipulated vector graphics, and created assets for both print and web.</p>
									</div>
									<div class="ff-12g ff-4-lg ff-hidden-sm">
										<div class="cb-sidebar">
										<img class="cb-img-responsive cb-z-depth-3" src="./img/adobe-illustrator-cc.svg"/>
										</div>
									</div>
								</div>

								<div class="ff-row cb-samples-title">
									<h2>Graphic Design Samples</h2>
								</div>
								<div class="ff-row cb-samples ff-center">
									<?php
									portfolioItem(
										// ID
										'designParastar',
										// Title
										'Parastar Billing',
										// Image
										'./img/sample-parastar-fire.jpg',
										// Secondary Link - use NULL if nothing
										null,
										// Body of popup
										"<h2>EMS Billing Rebranding </h2>

										<p>In late 2013 my department was charged with creating totally new marketing materials for spurring interest in Parastar’s turn-key EMS Billing services, which provides a comprehensive documentation and billing solution for municipal fire departments.</p>

										<p>I was tasked by my supervisor to create a new marketing identity, with simple messaging and a new color scheme - diverging from our traditional information-oriented briefing packets. The materials I created throughout 2014 incorporated an emphasis on firefighters, utilizing yellows and reds to replace the blue, white and gold we’ve traditionally used, along with bold fonts and simple to-the-point messaging.</p>

										<p>Branded materials for this campaign included 6’ tall pull-up banners, one-page handouts, pamphlets, and branded giveaway bags for distribution at events.</p>

										</p>",
										// Sidebar of popup (optional)
										'<table>
											<tr><img src="./img/design-parastar/Firefighter-Ad-2014b.jpg" class="cb-img-two-thrid cb-img-center cb-z-depth-2"/></tr>
											<tr>
												<th>Project</th>
												<td>Parastar Billing Campaign</td>
											</tr>
											<tr>
												<th>Campaign Date</th>
												<td>May 2014</td>
											</tr>
											<tr>
												<th>Software Used</th>
												<td>

														Adobe Illustrator CS5<br/>
														Adobe Photoshop CS5

												</td>
												<tr>
													<th>Resources Used</th>
													<td>iStock Photo<br/>
													Adobe Typekit Fonts</td>
												</tr>
												<tr>
													<th>Project Samples</th>
													<td><a href="./samples/Firefighter-Ad-2014b.pdf" target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i> Sample 1</a><br/>
													<a href="./samples/Fire-Sell-Sheet-4.pdf" target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i> Sample 2</a><br/>
													<a href="./samples/Parastar-full-page-ad-cmyk-bleed.pdf" target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i> Sample 3</a></td>
												</tr>
										</table>'
									);
									portfolioItem(
										// ID
										'designYoumaPrint',
										// Title
										'Youmacon',
										// Image
										'./img/sample-youmacon-print.jpg',
										// Secondary Link - use NULL if nothing
										null,
										// Body of popup
										"<h2>Youmacon Print and Web marketing</h2>

											<p>In addition to creating and maintaining the web presence for Youmacon, my department was also in charge of the company’s branding standards, which I communicated to outside vendors, press and publicists.  As we transitioned from being one of the fastest growing events to one of the largest in the world, it was important to make sure our branding continued adhering to the highest standards.</p>

											<p>Along with my volunteer position as Director of Media, I also preformed contracted work for Youmacon in the form of various graphics for several campaigns.  These campaigns include banners, social media graphics, handouts, advertisements, and other promotional materials.</p>

											<p>Additionally, I created the brand identity and logo for the company’s second event, the Midwest Media Expo.  I was active in the marketing of this event in 2013 and 2016. </p>
",
										// Sidebar of popup (optional)
										'<table>
											<tr><img src="./img/sample-youma-newyear.jpg" class="cb-img-two-thrid cb-img-center cb-z-depth-2"/></tr>
											<tr>
												<th>Client</th>
												<td>Youmacon</td>
											</tr>
											<tr>
												<th>Campaign Date</th>
												<td>2011 - Present</td>
											</tr>
											<tr>
												<th>Software Used</th>
												<td>
														Adobe Illustrator CS5 - CC<br/>
														Adobe Photoshop CS5 - CC

												</td>
												<tr>
													<th>Resources Used</th>
													<td>Even provided photos<br/>
													Vector logos from 2005
												</tr>
												<tr>
													<th>Project Samples</th>
													<td>
													<a href="./samples/NewYear16-2.jpg" target="_blank"><i class="fa fa-fw fa-file-image-o"></i>Youmacon 2016 Facebook Ad</a><br/>
													<a href="./samples/Todd-Haberkorn.png" target="_blank"><i class="fa fa-fw fa-file-image-o"></i>Youmacon 2014 Facebook Ad</a><br/>
													<a href="./samples/MCCC-promo-back.pdf" target="_blank"><i class="fa fa-fw fa-file-pdf-o"></i>M2X 2014 Print Ad</a><br/>
													<a href="./samples/todd-haberkorn-m2x.jpg" target="_blank"><i class="fa fa-fw fa-file-image-o"></i>M2X 2014 Facebook Ad</a>
												</tr>
										</table>'
									);

									?>
								</div>
							</div>
						</div>

						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-content-secondary ff-container cb-blue-bg" id=creativeCloud>

								<div class="ff-row">
									<div class="ff-12g ff-8-lg">
										<h1>Adobe Creative Cloud</h1>
										<p>As the most heavily used software packages in graphic design, proficiency with the Adobe Creative Cloud is essential to any graphic designer or web designer.  From Photoshop to generate images and assets, to Illustrator to create vector graphics for high-pixel-density displays, understanding this software suite is essential.</p>

										<p>The Files below demonstrate my proficiency with various tools in the Adobe Creative Cloud suite, including:</p>

										<ul>
											<li>Adobe Photoshop for pixel graphics</li>
											<li>Adobe Illustrator for vector graphics</li>
											<li>Adobe Audition for podcasts and audio</li>
											<li>Adobe Premiere for video editing</li>
										</ul>

									</div>
									<div class="ff-12g ff-4-lg ff-hidden-sm">
										<div class="cb-sidebar">
											<img class="cb-img-responsive" src="./img/creative-cloud-cc-2.svg"/>
										</div>
									</div>
								</div>
								<div class="ff-row cb-samples-title">
									<h2>Creative Cloud Project Samples</h2>
								</div>
								<div class="ff-row cb-samples ff-center">
									<div class="ff-3g ff-6-md ff-3-lg ff-6-sm">
										<a href="https://www.youtube.com/watch?v=nO5ygHSk7Us" target="_blank"><img class="cb-img-responsive cb-img-center cb-z-depth-2"src="./img/sample-youtube.jpg"/></a>
										<div class="ff-text-center">
											<a href="https://www.youtube.com/watch?v=nO5ygHSk7Us" target="_blank"><i class="fa fa-fw fa-youtube"></i>&nbsp;Premiere sample on YouTube</a>
										</div>
									</div>
									<div class="ff-3g ff-6-md ff-3-lg ff-6-sm">
										<a href="https://namenotfinal.net/podcast/ces-part-2-revenge-of-the-crap/" target="_blank"><img class="cb-img-responsive cb-img-center cb-z-depth-2"src="./img/nnf-ces2.jpg"/></a>
										<div class="ff-text-center">
											<a href="https://namenotfinal.net/podcast/ces-part-2-revenge-of-the-crap/" target="_blank"><i class="fa fa-fw fa-microphone"></i>&nbsp;Audition sample on Name Not Final (Podcast)</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<?php include_once "./php/cb-footer.php"; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- </div> CB Page Wrap -->

		<!-- Modals -->
<?php
		foreach ($pageModals as $modal) {
    echo $modal;
}
		?>

		<!-- <?php include_once "./php/js-end.php";?> -->
		<script type="text/javascript">
		  $(".parallax").scroll(function(){
		    var intViewportHeight = (window.innerHeight * 0.4) - 50; //percentage of div vs window
		    var halfViewportHeight = (window.innerHeight * 0.2);
		    t = (intViewportHeight - $(this).scrollTop())/100;
		    x = (halfViewportHeight - $(this).scrollTop())/100;
		    if(t<0)t=0;
		    if(x<0) x=0;
		    v = (1 - t);
		    $('.cb-nav-bg').css({opacity: v});
		    $('.cb-logo').css({opacity: v});
		    $('.cb-main-logo').css({opacity: x});
		  })

		</script>
		<script type="text/javascript">
		  $('.js-menuToggle').click(function(){
		     $(".cb-nav-hidden").fadeToggle().css("display","flex");
		  });
		</script>
	</body>
</html>
