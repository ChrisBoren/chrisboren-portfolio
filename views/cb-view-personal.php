<!DOCTYPE html>
<html>
	<?php
		$pageDescription = 'Samples of my graphic design and web design abilities.';
		$pageName = 'About Chris';
		include_once "./php/cb-global.php";
		include_once "./php/cb-head.php"; ?>
	<body>
		<?php include_once "./php/google-analytics.php";
					include_once "./php/js-start.php";
					include_once "./php/cb-navbar.php"; ?>
		<!-- <div class="cb-pageWrap"> -->
		<div class="parallax">
			<div id="group2" class="parallax__group cb-size-50">
				<div class="parallax__layer parallax__layer--base ">
					<div class="title cb-main-logo">
						<img src="<?php echo $siteLogo;?>">
						<!-- <p>About me</p> -->
					</div>
				</div>
				<div class="parallax__layer parallax__layer--back cb-bg-personal">
					<div class="title"></div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
			<div id="cb-content" class="parallax__group cb-size-50">
				<div class="parallax__layer cb-paralax-top parallax__layer--base">
					<div class="cb-container-main ff-container ">
						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-main-content ff-container">
								<div class="ff-row">
									<div class="ff-12g ff-7-lg">
										<h1>New City, New Opportunities</h1>

										<p>After an unexpected opportunity arose for my girlfriend in the fall of 2015, we made the decision to leave our ancient home of Detroit, and traverse across one of the largest lakes on the planet to the brave new world… of Chicago.  We settled on an apartment in Evanston bordering Rogers Park, and began to discover what adventures new home would bring.</p>

										<p>Since leaving my full-time management position with <a href="http://www.parastar.net" rel="nofollow" target="_blank">Parastar</a> in October and moving to Chicagoland in November, I've been spending my time laying the groundwork for a long-term personal project, but with the goal to begin seeking opportunities in the Spring.  It’s time for me to move forward and see what Chicago has to offer.</p>
										<p>I've also been spending my time learning about the city, spending a lot of time in the Loop at the Harold Washington Library, Grant Park, and exploring the less known features of the city such as the Pedway.</p>

										<p>With that, I’m currently seeking new full-time employment opportunities in the Chicago area, easily accessible by the L or Metra lines from the North.  My most recent employer was a large healthcare system, and although being a corporate manager had its perks, this time around I’m looking for the right company where I could make an impact as a front-end developer, tapping into my years of experience in design, management, and server administration to offer a unique viewpoint to this role.  A newer technology company or marketing firm would be ideal, but I’m open to opportunities where my skills could make an impact. </p>

										<p>Please feel free to look over my resumé, and free to send me an email if you’re interested at
											<script type="text/javascript">
												var email = 'opportunities';
												var domain = 'chrisboren.net';
												document.write('<a href="mailto:' + email + '@' + domain + '">' + email + '@' + domain + '</a>');
											</script>
											</p>

									</div>
									<div class="ff-12g ff-5-lg ff-hidden-sm ff-hidden-md">
										<div class="cb-sidebar">
										<img class="cb-img-responsive" src="./img/my-work-map-2.svg"/>
										</div>
									</div>
								</div>
								<div class="ff-row">
									<div class="ff-12g">
										<br/>
										<p class="ff-text-center">
											<a href="./documents/Boren-Christopher-Resume.pdf" target="_blank" class="btn cb-btn"><i class="fa fa-fw fa-file-pdf-o"></i> &nbsp; Download my PDF resumé </a>
										</p><br/>
										<p class="ff-text-center">
											<script type="text/javascript">
												document.write('<a href="mailto:'+ email + '@' + domain +'" target="_blank" class="btn cb-btn"><i class="fa fa-fw fa-envelope-o"></i> &nbsp; Send me an email</a>')
											</script>

										</p>
										<p class="ff-text-center">
											<img class="cb-img-one-third" src="./img/chicago-stars-blue.svg" alt="Chicago Flag Stars (blue)" />
										</p> <br/>
									</div>
								</div>

							</div>
						</div>


						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-content-secondary ff-container cb-blue-bg">

								<div class="ff-row">
									<div class="ff-12g">

										<h1>Professional Work History</h1>

										<p>Throughout my experience I’ve been commended for my work by consistent advancement and increasing responsibilities. I hope to continue the same momentum of passion, energy, and commitment in my next professional position.  To keep my work history focused, I’ve excluded my beginnings as a manager in the food service industry.</p>

										<p>Professional references available upon request.</p>

										<!-- Parastar -->

										<div class="cb-company-profile ff-container">
											<div class="ff-row cb-company-header">
												<div class="ff-8g ff-12-sm">
													<h2 class="cb-company-title">Parastar, Inc</h2>
													<div class="cb-company-overview">
														<ul class="ff-space-between ff-content-stretch">
															<li>Web &amp; Multimedia Manager</li>
															<li>Farmington Hills, MI</li>
															<li> June 2012 &mdash; October 2015</li>
														</ul>
													</div>

												</div>
												<div class="ff-4g ff-hidden-sm">
													<img class="cb-company-logo" src="./img/parastar-logo.svg" alt="Parastar, Inc. Logo">
												</div>

											</div>

											<div class="ff-row cb-company-details ff-container">
												<div class="ff-3g ff-12-sm">
													<ul>
														<li>Project management of online initiatives</li>
														<li>New Business proposals &amp; RFP responses</li>
														<li>Branding standards</li>
														<li>Web, Digital, and Print Marketing</li>
													</ul>


												</div>
												<div class="ff-9g ff-12-sm">
												<p>Parastar is a global consultancy and EMS management firm that delivers revenue-generating solutions for EMS, fire departments and healthcare systems. Parastar manages the daily operations of eleven not-for-profit EMS companies nationwide, executes EMS billing solutions for both private and municipal EMS agencies, provides non-emergency and 9-1-1 medical dispatch services.  They consult worldwide on emergency medical services systems.</p>

												<p>Reporting directly to the Executive Vice President of Innovation and Business Ventures, my duties as the Web Design &amp; Multimedia Manager for Parastar included website design and development, graphic design, brand management, social media monitoring, online marketing, facilitation of new business proposals, and multimedia management for Parastar, Community EMS, and its nationwide network of affiliated ambulance companies.</p>
												</div>
											</div>


										</div>

										<!-- Comspec -->
										<div class="cb-company-profile ff-container">
											<div class="ff-row cb-company-header">
												<div class="ff-8g ff-12-sm">
													<h2 class="cb-company-title">ComSpec International</h2>
													<div class="cb-company-overview">
														<ul class="ff-space-between ff-content-stretch">
															<li>Support Technician</li>
															<li>Bingham Farms, MI</li>
															<li> July 2011 &mdash; June 2012</li>
														</ul>
													</div>

												</div>
												<div class="ff-4g ff-hidden-sm">
													<img class="cb-company-logo" src="./img/comspec-logo.gif" alt="Comspec International Logo">
												</div>

											</div>

											<div class="ff-row cb-company-details ff-container">
												<div class="ff-3g ff-12-sm">
													<ul>
														<li>Windows and Linux server administration</li>
														<li>Windows Domain / Active Directory Administration</li>
														<li>Front-end website development</li>
														<li>Desktop Support</li>
													</ul>


												</div>
												<div class="ff-9g ff-12-sm">
													<p>ComSpec International is a software development firm which creates custom solutions for Fortune 1000 companies across the globe. Their flagship product, EMPOWER, is the backbone of many college computer systems in the United States.</p>

													<p>As the Support Technician for ComSpec International, it was my duty to see to the needs of our off-site support clients and assist with IT administration at our Bingham Farms headquarters. Common tasks included managing Windows Server 2003 &amp; 2008 Active Directory environments, malware detection and removal, server backups, Oracle (6i &amp; 11g) databases and Cold Fusion virtual server setup and configurations, and troubleshooting client issues with Laptop &amp; Desktop PCs. Additional duties included creating and maintaining company websites, and creating front-end themes for EMPOWER.</p>
												</div>
											</div>


										</div>
										<!-- End Comspec -->

										<!-- Youmacon -->
										<div class="cb-company-profile ff-container">
											<div class="ff-row cb-company-header">
												<div class="ff-8g ff-12-sm">
													<h2 class="cb-company-title">Youmacon Enterprises</h2>
													<div class="cb-company-overview">
														<ul class="ff-space-between ff-content-stretch">
															<li>Director of Media</li>
															<li>Troy, MI</li>
															<li> September 2005 &mdash; December 2014</li>
															<li>(part-time commitment)</li>
														</ul>
													</div>

												</div>
												<div class="ff-4g ff-hidden-sm">
													<img class="cb-company-logo" src="./img/youmacon-logo.svg" alt="Youmacon Logo">
												</div>

											</div>

											<div class="ff-row cb-company-details ff-container">
												<div class="ff-3g ff-12-sm">
													<ul>
														<li>Website design and development</li>
														<li>Social media management</li>
														<li>Creation of marketing materials</li>
														<li>Branding standards</li>
													</ul>


												</div>
												<div class="ff-9g ff-12-sm">
													<p>Started by a group of friends in the Metro Detroit area in 2005, the animé and gaming convention known as Youmacon has become one of the largest and fastest growing events of its type in North America, surpassing the hotel room capacity of Superbowl XL and becoming the first and only event to fill all 1,298 rooms at Detroit Marriott at the Renaissance Center to its maximum capacity. In 2015 the event had an attendance of nearly 19,000 individuals (over 50,000 turnstile attendance) over the course of the event.</p>

													<p>As the Director of Media for Youmacon, it was my responsibility to oversee all marketing efforts. Responsibilities include website design and development, graphic design, image and brand management, managing convention photographers &amp; videographers, creating and enforcing branding standards, and increasing awareness of our event.</p>
												</div>
											</div>


										</div>
										<!-- End Youmacon -->

										<!-- Detroit Geeks -->
										<div class="cb-company-profile ff-container">
											<div class="ff-row cb-company-header">
												<div class="ff-8g ff-12-sm">
													<h2 class="cb-company-title">Detroit Geeks, LLC</h2>
													<div class="cb-company-overview">
														<ul class="ff-space-between ff-content-stretch">
															<li>President &amp; co-founder</li>
															<li>Warren, MI</li>
															<li> August 2009 &mdash; June 2011</li>
														</ul>
													</div>

												</div>
												<div class="ff-4g ff-hidden-sm">
													<!-- <img class="cb-company-logo" src="./img/null" alt="Detroit Geeks Logo"> -->
												</div>

											</div>

											<div class="ff-row cb-company-details ff-container">
												<div class="ff-3g ff-12-sm">
													<ul>
														<li>Co-Founder of business</li>
														<li>Hardware &amp; software support</li>
														<li>Website design &amp; Development</li>
														<li>Online marketing</li>
													</ul>


												</div>
												<div class="ff-9g ff-12-sm">
													<p>Detroit Geeks was a full service computer repair shop offering local residential and business clients quick-service computer repair at affordable prices. </p>

													<p>As the co-founder and lead technician, duties included diagnostics and repair of various hardware and software issues and network troubleshooting, website design, marketing, and management of company funds.  The company ceased operations in June 2011.</p>
												</div>
											</div>


										</div>
										<!-- End Detroit Geeks -->


																			<div class="ff-row">
																				<div class="ff-12g">
																					<br/><br/>
																					<p class="ff-text-center">
																						<a href="./documents/Boren-Christopher-Resume.pdf" target="_blank" class="btn cb-btn"><i class="fa fa-fw fa-file-pdf-o"></i> &nbsp; Download my PDF resumé </a>
																					</p><br/>
																					<p class="ff-text-center">
																						<script type="text/javascript">
																							document.write('<a href="mailto:'+ email + '@' + domain +'" target="_blank" class="btn cb-btn"><i class="fa fa-fw fa-envelope-o"></i> &nbsp; Send me an email</a>')
																						</script>

																					</p><br/>
																				</div>
																			</div>



									</div>
								</div>
							</div>
						</div>

						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-content-secondary ff-container">

								<div class="ff-row">
									<div class="ff-12g">

										<h1>Online Radio Show: Name Not Final</h1>

										<img src="./img/NNF-CES-2493.jpg" class="cb-img-right cb-z-depth-2" alt="Chris Boren at CES 2016" /><p>Along with my work formal employment and contract work with development and design, I also have several side projects and hobbies which keep me occupied, such as producing and co-hosting an online radio show, traveling across the country, and gaming.</p>

										<p>In 2013 I was given an opportunity to create a new online radio show with my friends for Las Vegas based 91.8 The Fan to discuss the Video Game, Tech, and Entertainment industries.  Made up of experts in consumer technology, video games, and entertainment the three of us began Name Not Final.</p>

										<p>As a part of Name Not Final, we have had the opportunity to travel to many events around the country, such as <a target="_blank" rel="nofollow" href="http://www.comic-con.org/">San Diego Comic Con</a>, <a target="_blank" rel="nofollow" href="http://thegameawards.com/">The Game Awards</a>, <a target="_blank" rel="nofollow" href="https://www.ces.tech/">CES</a>, <a target="_blank" rel="nofollow" href="http://www.newyorkcomiccon.com/">NYCC</a>, and many more.  Our live coverage often varies from on-location interviews to comic bits from hotel lobbies.  We often put a sarcastic comedic twist on the week’s news, and even try to set each other off on rants about disheartening industry trends, but at the end of the day we’ve had a lot of fun with this project.</p>

										<p>The show is currently in hiatus due to scheduling conflicts as we advance our careers, however we plan to transition from radio to podcast later this year, allowing us more flexibility in recording and editing.</p>

										<p>Not only have I co-hosted this show for over 2 years, but I have also served as its editor, recording all audio for our broadcast and editing in Adobe Audition to reduce background noise, edit out problems, and reduce ‘dead air’ between topics.  As an early tool to prep for our transition from Radio to Podcast I’ve also set up <a href="https://namenotfinal.net" target="_blank">namenotfinal.net</a> to be a placeholder episode list and podcast feed generator. </p>

										<!-- <img src="./img/nnf-logo.svg" class="cb-img-center cb-img-one-third cb-z-depth-2" /> -->

										<!-- <h2>Gaming</h2>

										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> -->

										<!-- <img src="./img/starcraft.png" class="cb-img-responsive" /> -->

										<p class="ff-text-center"><br/>
											<a href="https://namenotfinal.net" target="_blank" class="btn cb-btn"><i class="fa fa-fw fa-external-link-square"></i> &nbsp; Visit Name Not Final</a>
										</p>

									</div>
								</div>
							</div>
						</div>

						<div class="clear"></div>
						<?php include_once "./php/cb-footer.php"; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- </div> CB Page Wrap -->

		<!-- Modals -->


		<script type="text/javascript">
		  $(".parallax").scroll(function(){
		    var intViewportHeight = (window.innerHeight * 0.4) - 50; //percentage of div vs window
		    var halfViewportHeight = (window.innerHeight * 0.2);
		    t = (intViewportHeight - $(this).scrollTop())/100;
		    x = (halfViewportHeight - $(this).scrollTop())/100;
		    if(t<0)t=0;
		    if(x<0) x=0;
		    v = (1 - t);
		    $('.cb-nav-bg').css({opacity: v});
		    $('.cb-logo').css({opacity: v});
		    $('.cb-main-logo').css({opacity: x});
		  })

		</script>
		<script type="text/javascript">
		  $('.js-menuToggle').click(function(){
		     $(".cb-nav-hidden").fadeToggle().css("display","flex");
		  });
		</script>
	</body>
</html>
