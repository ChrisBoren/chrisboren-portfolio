<footer class="ff-row ff-center">
		<div class="cb-container-fixed ff-container">
			<div>
				<p>Unless otherwise noted, original content and code copyright 2001 - 2016 Christopher Boren, All Rights Reserved.<br/>
				Want to take a look at the PHP of this page? Awesome! I've prepared a <a href="https://gitlab.com/ChrisBoren/chrisboren-portfolio" target="_blank">git repo</a> for you.
				</p>


			</div>
		</div>
</footer>
