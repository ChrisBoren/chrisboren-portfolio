<?php

function pageModal($id, $title, $body, $sidebar = null, $image=null){

$pageModals = array();


$part1 = '

<div class="modal fade" id="'.$id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog cb-container-fixed ff-container" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle fa-fw"></i> &nbsp; ' .$title. '</h4>
      </div>
      <div class="modal-body">

				<div class="ff-row">';
			if ($sidebar != null){
				$part2 =
					'<div class="ff-push-1 ff-4g ff-12-sm">
						'.$sidebar.'
					</div>
					<div class="ff-push-1 ff-5g ff-12-sm">


				';
			}
			else{
				$part2 = '
				<div class="ff-row">
					<div class="ff-push-1-sm ff-10-sm">';
			}

      $part3 = $body.'
			</div>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="cb-btn cb-btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>';
$thisModal = $part1 . $part2 . $part3;
return $thisModal;


};

function portfolioItem($id, $title, $image, $secondaryLink, $body, $sidebar = null){
	echo '
		<div class="ff-3g ff-6-md ff-3-lg ff-6-sm">
			<a href="" data-toggle="modal" data-target="#'.$id.'"><img class="cb-img-responsive cb-img-center" src="'.$image.'" alt="'.$title.'" title="'.$title.'"/></a>
			<div class="ff-text-center">
				<a href="" data-toggle="modal" data-target="#'.$id.'"><i class="fa fa-fw fa-info-circle"></i>&nbsp;More Info</a>';
				if ($secondaryLink != null){
					echo '| '. $secondaryLink;
				}
				 echo '
			</div>
		</div>';
		// store in $pageModals to call at end of HTML
		global $pageModals;
		$modal = pageModal($id, $title, $body, $sidebar, $image);
		$pageModals[] = $modal;
};




?>
