<script type="text/javascript">
  $(".parallax").scroll(function(){
    var intViewportHeight = (window.innerHeight * 0.7) - 50; //percentage of div vs window
    var halfViewportHeight = (window.innerHeight * 0.4);
    t = (intViewportHeight - $(this).scrollTop())/200;
    x = (halfViewportHeight - $(this).scrollTop())/200;
    if(t<0)t=0;
    if(x<0) x=0;
    v = (1 - t);
    $('.cb-nav-bg').css({opacity: v});
    $('.cb-logo').css({opacity: v});
    $('.cb-main-logo').css({opacity: x});
  })

</script>
<script type="text/javascript">
  $('.js-menuToggle').click(function(){
    // $( ".cb-nav-overlay" ).toggleClass( "cb-nav-hidden", 500 );

    $(".cb-nav-hidden").fadeToggle().css("display","flex");
    //$( ".cb-nav-hidden" ).fadeToggle( "fast", "linear" );
  });
</script>
