
<!-- For IE users -->
<script type="text/javascript">
function msieversion() {

var ua = window.navigator.userAgent;
var msie = ua.indexOf("MSIE ");

if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
{
//alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
alert("You're using Internet Explorer! Although I'd never use code like this in production without a fallback, this portfolio is coded to showcase flexbox, svg graphics, 3DCanvas, and a few other things not quite support here. Try visiting this site in Microsoft Edge, Microsoft’s latest browser which supports these methods.");

$( ".cb-ie-window" ).addClass( "cb-showDiv" );
}
else  // If another browser, return 0
{
//alert('otherbrowser');
}

return false;
}

msieversion();
</script>
