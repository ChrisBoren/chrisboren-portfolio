<?php
// Global settings
$siteName = "The Portfolio of Chris Boren";
$siteURL = $_SERVER['SERVER_NAME'];
$siteLogo = "./img/cb-handwritten.svg";
// Additonal PHP to include on each page
include_once "./php/cb-modals.php";

$sitePath = 'http://'.$siteURL;
if (isset($_SERVER['HTTPS'])) {
  $sitePath = 'https://'.$siteURL;
}

?>
