<head>
	<meta charset="utf-8">
	<base href="<?php echo $sitePath; ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php echo $siteName . ' - ' . $pageName; ?></title>
	<meta name="author" content="Chris Boren">
	<link rel="stylesheet" type="text/css" href="./css/style.css?revision=2016-06-15">
	<!-- Favicon & OS Icons -->
	<!-- Government mandated Apple icons -->
	<link rel="apple-touch-icon" sizes="57x57" href="./img/icons/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="./img/icons/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="./img/icons/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="./img/icons/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="./img/icons/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="./img/icons/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="./img/icons/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="./img/icons/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="./img/icons/apple-icon-180x180.png">
	<!-- Android / Generic Icons (for those not-intiated in the ways of Jobs and Woz) -->
	<link rel="icon" type="image/png" sizes="192x192"  href="./img/icons/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="./img/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="./img/icons/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="./img/icons/favicon-16x16.png">
	<link rel="manifest" href="./img/icons/manifest.json">
	<!-- Microsoft Browsers / Win 10 -->
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="./img/icons/ms-icon-144x144.png">
	<!-- Theme color for Chrome / Android / future use -->
	<meta name="theme-color" content="#0D47A1">
	<!-- Twitter Cards -->
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@ChrisBoren">
	<meta name="twitter:title" content="The Portfolio of Chris Boren">
	<meta name="twitter:description" content="View graphic design projects, code samples, and live websites that i've created.  Hey look, a Twitter card!">
	<meta name="twitter:creator" content="@ChrisBoren">
	<!-- Twitter summary card with large image must be at least 280x150px -->
	<!-- 	<meta name="twitter:image:src" content="http://www.example.com/image.html"> -->
	<!-- Open Graph -->
	<meta property="og:title" content="<?php echo $pageName; ?> - The Portfolio of Chris Boren" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://<?php echo $siteURL ?>" />
	<meta property="og:image" content="http://<?php echo $siteURL . '/img/og-portfolio.png';?>" />
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="630" />

</head>
