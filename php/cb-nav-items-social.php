<li><a target="_blank" href="https://twitter.com/chrisboren"><i title="@ChrisBoren on Twitter"class="fa fa-fw fa-twitter"></i></a></li>
<!-- <li><a target="_blank" href="https://plus.google.com/+chrisboren" rel="author"><i title="+ChrisBoren on Google Plus"class="fa fa-fw fa-google-plus"></i></a></li> -->
<li><a target="_blank" href="https://www.linkedin.com/in/chrisboren"><i title="ChrisBoren on Linkedin"class="fa fa-fw fa-linkedin"></i></a></li>
<li><a target="_blank" href="https://github.com/chrisboren"><i title="ChrisBoren on GitHub"class="fa fa-fw fa-github"></i></a></li>
<li><a target="_blank" href="https://gitlab.com/u/chrisboren"><i title="ChrisBoren on GitLab"class="fa fa-fw fa-gitlab"></i></a></li>
