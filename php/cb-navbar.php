<nav>
  <div class="ff-container cb-container-fixed">
    <div class="ff-row">
      <div class="ff-1-sm ff-column ff-items-center ff-content-stretch ff-1-md cb-navigation ff-hidden-lg ff-hidden-xl">
          <div class="ff-text-center ff-100p ff-content-center ff-flex ff-column ff-jusitfy">
            <p class="ff-text-center ff-100p ff-vertical-center">
              <a class="js-menuToggle">
                <i class="fa fa-fw fa-bars"></i>
              </a>
            </p>
          </div>
      </div>

      <div class="ff-4-lg ff-10-md ff-10-sm">
        <div class="cb-logo ff-hidden-sm ff-hidden-md"><img src="./img/cb-handwritten.svg"></div>
        <div class="cb-logo ff-hidden-lg ff-hidden-xl ff-items-center"><p class="ff-text-center ff-100p"><img src="./img/cb-handwritten.svg"></p></div>
      </div>



      <div class="ff-8-lg cb-navigation ff-hidden-sm ff-hidden-md">
        <ul class="ff-space-around ff-content-stretch">
        <?php include "./php/cb-nav-items.php";
              include "./php/cb-nav-items-social.php"; ?>

          <!-- </div> -->
        </ul>
      </div>



    </div>
  </div>
</nav>

<script type="text/javascript">
  // Quickly set color effects for menu hover using exsisting CSS.
  $('.fa-twitter').parent('a').parent('li').addClass('cb-twitter');
  $('.fa-google-plus').parent('a').parent('li').addClass('cb-google-plus');
  $('.fa-github').parent('a').parent('li').addClass('cb-github');
  $('.fa-linkedin').parent('a').parent('li').addClass('cb-linkedin');
  $('.fa-gitlab').parent('a').parent('li').addClass('cb-gitlab');
</script>


<!-- Begin Mobile Nav Overlay -->
<div class="cb-nav-bg"></div>
<div class="cb-nav-overlay cb-nav-hidden ff-container ff-items-center">
  <div class="ff-container cb-container-fixed">
    <div class="ff-row">
      <div class="ff-1-sm ff-column ff-items-center ff-content-stretch ff-1-md cb-navigation ff-hidden-lg ff-hidden-xl">
          <div class="ff-text-center ff-100p ff-content-center ff-flex ff-column ff-jusitfy">
            <p class="ff-text-center ff-100p ff-vertical-center">
              <a class="js-menuToggle">
                <i class="fa fa-fw fa-times"></i>
              </a>
            </p>
          </div>
      </div>
    </div>
  </div>

  <div class="cb-nav-overlay-menu ff-column">
    <ul class="ff-space-around ff-content-stretch">
      <li><img src="<?php echo $siteLogo;?>" class="cb-img-center cb-img-responsive"></li>
      <?php include "./php/cb-nav-items.php"; ?><br/>
      <!-- <div class="cb-navigation ff-content-stretch ff-space-between"> -->
      <div class="ff-row ff-space-between">
        <?php include "./php/cb-nav-items-social.php";?>
      </div>
      <!-- </div> -->
    </ul>
  </div>


</div>

<!-- End Nav Overlay -->


<!-- Begin BatCobwebs -->
<style>
	.batOverlay{
		z-index:-10;
		opacity:0;
		position: fixed;
		top:0;
		left:0;
		width:100%;
		height:100%;
		background-color:rgba(0,0,0,.8);
		display:flex;
		flex-direction:row;
		transition: all .5s ease-in-out, z-index 0s linear .5s;
		font-family:"Open Sans", sans-serif;
		font-size:16px;
		align-items: center;
		justify-content: center;
	}
	.batOverlay.active{
		z-index:9999;
		opacity:1;
		transition: all 1s ease-in-out, z-index 0s linear;
	}
	.batOverlay h1{
		line-height:1em;
		letter-spacing:-.05em;
	}
	.batOverlay .batContents{
		background-color:white;
		/* display:none; */
		max-width:900px;
		width:100%;
		padding:1rem 2rem;
		border-radius:1rem;
		min-height:400px;
		background-image:url('batman-and-robin.jpg');
		background-size:80%;
		background-repeat:no-repeat;
		background-position:bottom right;
		transform:translate3d(0px, 100px, 0px);
		transition:all 1s ease-in-out;
		opacity:0;
	}
	.batOverlay.active .batContents{
		transform:translate3d(0px, 0px, 0px);
		opacity:1;
	}
	@media screen and (max-width:910px){
		.batOverlay .batContents{
			background-size:50%;
		}
	}
	.batOverlay .batContents .batText{
		max-width:31em;
	}
	.batBtn{
		display:inline-block;
		padding:.25em 1em;
		background-color:#fffc00;
		border:2px solid black;
		color:black!important;
		font-weight:bold;
		text-align:center;
		text-transform: uppercase;
		user-select: none;
		cursor: pointer;
		transition:all .25s ease-in-out!important;
		transform: translate3d(0px, 0px, 0px);
		text-decoration: none!important;
    margin-top:2rem;
		letter-spacing:-.025em;
	}
	.batBtn:hover{
		transform: translate3d(0px, -.25em, 0px);
		box-shadow:0px 3px 9px rgba(0,0,0,.25);
	}
	.batCenter{
		text-align:center;
	}
	</style>
	
	<div class="batOverlay">
		<div class="batContents">
			<div class="batText">
				<h1>Holy cobwebs, Batman!  <br/>This portfolio is nearly 3 years old!</h1>
				<p>It's been a while since I’ve updated my portfolio — at the time it was created I relied heavily on jQuery, barely touched SASS, and FlexBox had sketchy support. Oh, how the times have changed! I'll be updating my portfolio this summer, but in the meantime please feel free to take a look around this museum.</p>
				<!-- <p style="margin-top:1rem;">-Chris</p> -->
				<p class="batCenter"><a class="batBtn closeBatOverlay">Continue to site</a></p>
			</div>
		</div>
	</div>

<script>

	// Cookies!
	// Bake the cookie
	var createCookie = function(name, value, days) {
		var expires;
		if (days) {
				var date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
		}
		else {
				expires = "";
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	};
	// Eat the cookie
	function getCookie(c_name) {
		if (document.cookie.length > 0) {
				c_start = document.cookie.indexOf(c_name + "=");
				if (c_start != -1) {
						c_start = c_start + c_name.length + 1;
						c_end = document.cookie.indexOf(";", c_start);
						if (c_end == -1) {
								c_end = document.cookie.length;
						}
						return unescape(document.cookie.substring(c_start, c_end));
				}
		}
		return "";
	}

	document.addEventListener('DOMContentLoaded', function(){
		let batClose = document.querySelector('.batBtn');
		let batOverlay = document.querySelector('.batOverlay');
		batClose.addEventListener('click', function(){
			createCookie('batCookie', true, .004);
			batOverlay.classList.remove('active');
		});
		batmanHasCookie = getCookie('batCookie');
		if(!batmanHasCookie){
			// setTimeout(() => {
				batOverlay.classList.add('active');
			// }, 1000);
		};

	});
	// function clearBatCookie(){
	// 		createCookie('batCookie', false, .0001);
	// }
</script>
<!-- End Bat Cobwebs -->