<!DOCTYPE html>
<html>
	<?php
		$pageDescription = 'Samples of my graphic design and web design abilities.';
		$pageName = 'Design';
		include_once "./php/cb-global.php";
		include_once "./php/cb-head.php"; ?>
	<body>
		<?php include_once "./php/js-start.php";
					include_once "./php/cb-navbar.php"; ?>
		<!-- <div class="cb-pageWrap"> -->
		<div class="parallax">
			<div id="group2" class="parallax__group cb-size-50">
				<div class="parallax__layer parallax__layer--base ">
					<div class="title cb-main-logo">
						<!-- <img src="<?php echo $siteLogo;?>"> -->
						<h1>404 - Page Not Found</h1>
					</div>
				</div>
				<div class="parallax__layer parallax__layer--back cb-bg-404">
					<div class="title"></div>
				</div>
			</div>
			<div class="clear">&nbsp;</div>
			<div id="cb-content" class="parallax__group cb-size-50">
				<div class="parallax__layer cb-paralax-top parallax__layer--base">
					<div class="cb-container-main ff-container ">
						<div class="ff-row ff-center">
							<div class="cb-container-fixed cb-main-content ff-container">
								<div class="ff-row">
									<div class="ff-12g ff-10-lg ff-push-1-lg">
										<h1>Oh No! A Wild 404 Appeared!</h1>
										<p>You tried to load <i><?php echo $siteURL; echo $_SERVER['REQUEST_URI']; ?></i>, but ended up here with a 404 error.  How unfortunate.</p>

										<p>The 404 or Not Found error message is an HTTP standard response code indicating that the client was able to communicate with a given server, but the server could not find what was requested.  Basically, the page you're looking for dosen't exist.</p>

										<p>In the wise words of Douglas Adams: <strong>Don't Panic</strong>. Getting home is <a href="http://<?php echo $siteURL;?>">always a click away</a>.</p>
										<!-- https://youtu.be/tTNuldPhP20?t=20m56s -->
									</div><!--
									<div class="ff-12g ff-4-lg ff-hidden-sm">
										<div class="cb-sidebar">
										<img class="cb-img-responsive" style="-webkit-filter: sepia(0.75); border:1px solid rgba(0,0,0,0.25);" src="./img/missingNo.png"/>
										</div>
									</div> -->
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<?php include_once "./php/cb-footer.php"; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- </div> CB Page Wrap -->

		<!-- Modals -->
		<?php pageModal('designYouma', 'Youmacon 2016: Commissioned Website Project', "<p>Started by a group of friends in the Metro Detroit area in 2005, the anime and gaming convention known as Youmacon has become one of the largest and fastest growing events of its type in North America, outselling the hotel rooms of Superbowl XL and managing to become the first and only single event to fill the Detroit Marriott at the Renaissance Center - the tallest hotel in the western hemisphere - to its maximum capacity. To accommodate it's rapid growth, Youmacon expanded its event to include Downtown Detroit's Cobo Center in 2012.  In 2014 the event had an attendance of nearly 17,000 individuals (or over 45,000 turnstile attendance) over the course of the event. </p><p>I have been designing and implementing Youmacon's website since 2011.</p>"); ?>
		<?php pageModal('designDMCare', 'DMCare Express Website: part of the Detroit Medical Center', "<p>Started by a group of friends in the Metro Detroit area in 2005, the anime and gaming convention known as Youmacon has become one of the largest and fastest growing events of its type in North America, outselling the hotel rooms of Superbowl XL and managing to become the first and only single event to fill the Detroit Marriott at the Renaissance Center - the tallest hotel in the western hemisphere - to its maximum capacity. To accommodate it's rapid growth, Youmacon expanded its event to include Downtown Detroit's Cobo Center in 2012.  In 2014 the event had an attendance of nearly 17,000 individuals (or over 45,000 turnstile attendance) over the course of the event. </p><p>I have been designing and implementing Youmacon's website since 2011.</p>"); ?>

		<!-- <?php include_once "./php/js-end.php";?> -->
		<script type="text/javascript">
		  $(".parallax").scroll(function(){
		    var intViewportHeight = (window.innerHeight * 0.4) - 50; //percentage of div vs window
		    var halfViewportHeight = (window.innerHeight * 0.2);
		    t = (intViewportHeight - $(this).scrollTop())/100;
		    x = (halfViewportHeight - $(this).scrollTop())/100;
		    if(t<0)t=0;
		    if(x<0) x=0;
		    v = (1 - t);
		    $('.cb-nav-bg').css({opacity: v});
		    $('.cb-logo').css({opacity: v});
		    $('.cb-main-logo').css({opacity: x});
		  })

		</script>
		<script type="text/javascript">
		  $('.js-menuToggle').click(function(){
		     $(".cb-nav-hidden").fadeToggle().css("display","flex");
		  });
		</script>
	</body>
</html>
